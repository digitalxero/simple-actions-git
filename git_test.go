package git_test

import (
	"os"

	"gitlab.com/digitalxero/simple-actions/term"
	"sigs.k8s.io/kind/pkg/log"
)

var logger = term.NewLogger(os.Stderr, log.Level(3))

const (
	gitlabDomain = "gitlab.com"
)
