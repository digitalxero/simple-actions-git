package git_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	actions "gitlab.com/digitalxero/simple-actions"

	gitActions "gitlab.com/digitalxero/simple-actions-git"
)

func TestGetAllTagsGitlabSingleProjectNoPreRelease(t *testing.T) {
	// https://gitlab.com/digitalxero/semrel-test-repo-1
	gitAuth, err := gitActions.NewNoAuth()
	require.NoError(t, err)
	gitClient := gitActions.NewGitlabClient().WithDomain(gitlabDomain).WithAuth(gitAuth)
	actionCTX := gitActions.NewDefaultActionContext(logger, false).WithClient(gitClient)

	getTagsAction := &actions.Actions{
		gitActions.NewGetTagsAction("23365984"),
		gitActions.NewSortTagsAction(true),
	}
	err = getTagsAction.Execute(actionCTX)
	require.NoError(t, err)
	tags := actionCTX.Tags()
	require.Len(t, tags, 55)
}
