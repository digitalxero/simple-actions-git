package git

import (
	"fmt"

	"github.com/AlekSi/pointer"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/digitalxero/simple-actions"
)

type getCommitsAction struct {
	*baseGitAction
	start,
	end string
}

// ZeroHash is the null hash for git
const ZeroHash = "0000000000000000000000000000000000000000"

func NewGetAllCommits(projectID string) actions.Action {
	return &getCommitsAction{
		&baseGitAction{projectID, "", ""},
		ZeroHash, "",
	}
}

func NewGetCommitsBetween(projectID, start, end string) actions.Action {
	a := NewGetAllCommits(projectID).(*getCommitsAction)
	a.start = start
	a.end = end

	return a
}

func NewGetCommitsBetweenForSubproject(projectID, subprojectName, start, end string) actions.Action {
	a := NewGetCommitsBetween(projectID, start, end).(*getCommitsAction)
	a.subprojectName = subprojectName

	return a
}

func NewGetCommitsBetweenForBranch(projectID, branch, start, end string) actions.Action {
	a := NewGetCommitsBetween(projectID, start, end).(*getCommitsAction)
	a.branch = branch

	return a
}

func NewGetCommitsBetweenForSubprojectOnBranch(projectID, subprojectName, branch, start, end string) actions.Action {
	a := NewGetCommitsBetween(projectID, start, end).(*getCommitsAction)
	a.subprojectName = subprojectName
	a.branch = branch

	return a
}

// Execute docs
func (a *getCommitsAction) Execute(ctx actions.ActionContext) (err error) {
	var (
		ok      bool
		gctx    gitActionContext
		commits Commits
	)

	if gctx, ok = ctx.(gitActionContext); !ok {
		return fmt.Errorf("wrong context type: %T", ctx)
	}

	ctx.Status().Start(fmt.Sprintf("Fetching Project %s commits", a.projectID))
	defer func() {
		ctx.Status().End(err == nil)
	}()
	if ctx.IsDryRun() {
		return nil
	}


	switch client := gctx.Client().(type) {
	case GitlabClient:
		err = a.executeGitlab(gctx, client)
	default:
		// do local
	}

	if err != nil {
		return err
	}

	if a.start == ZeroHash && a.end == "" {
		// no filters set, so commits are already filtered
		return nil
	}

	commits = gctx.Commits()
	if a.start == "" {
		a.start = commits[0].SHA
	}

	var filteredCommits = make(Commits, 0)
	for idx, commit := range commits {
		switch {
		case commit.SHA == a.end:
			break
		case  commit.SHA != a.start:
			continue
		case len(commits)-1 > idx:
			a.start = commits[idx+1].SHA
		}

		filteredCommits = append(filteredCommits, commit)
	}

	gctx.WithCommits(filteredCommits)

	return nil
}

func (a *getCommitsAction) executeGitlab(ctx gitActionContext, cl GitlabClient) (err error) {
	var (
		client *gitlab.Client
		commits Commits
		glCommits, retCommits []*gitlab.Commit
		listOptions = &gitlab.ListCommitsOptions{ListOptions: gitlab.ListOptions{PerPage: 1000}}
		page = 1
	)
	if client, err = cl.Client(); err != nil {
		return err
	}

	if a.subprojectName != "" {
		listOptions.Path = pointer.ToString(fmt.Sprintf("%s/*", a.subprojectName))
	}

	if a.branch != "" {
		listOptions.RefName = pointer.ToString(a.branch)
	}

	for {
		listOptions.ListOptions.Page = page
		if retCommits, _, err = client.Commits.ListCommits(a.projectID, listOptions); err != nil {
			return err
		}
		glCommits = append(glCommits, retCommits...)
		page++
		if len(retCommits) < 1000 {
			break
		}
	}
	commits = make(Commits, 0)
	commits.FromGitlab(glCommits, client)
	ctx.WithCommits(commits)
	return nil
}