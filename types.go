package git

import (
	"path"
	"time"

	semver "github.com/hashicorp/go-version"
)

type GitAuth int

const (
	NoAuth GitAuth = iota
	BasicAuth
	NetRCAuth
	OAuth
)

type Tag struct {
	Name    string
	SHA     string
	Version *semver.Version
}
type Tags []*Tag

type Commit struct {
	SHA     string
	Message string
	Time    *time.Time
	// File Paths touched by this commit
	Files []string
}
type Commits []*Commit

// Len return the number of messages in the slice
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Len() int {
	return len(v)
}

// Less reports whether the element with
// index i should sort before the element with index j.
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Less(i, j int) bool {
	var (
		err error
		v1  = v[i].Version
		v2  = v[j].Version
		t1  = v[i].Name
		t2  = v[j].Name
	)

	if v1 == nil {
		if v1, err = semver.NewVersion(t1); err != nil {
			tagPrefix := path.Dir(t1)
			if tagPrefix == "." {
				tagPrefix = ""
			}
			tagVersion := path.Base(t1)
			if v1, err = semver.NewSemver(tagVersion); err != nil {
				v1 = &semver.Version{}
			}
		}
	}

	if v2 == nil {
		if v2, err = semver.NewVersion(t2); err != nil {
			tagPrefix := path.Dir(t2)
			if tagPrefix == "." {
				tagPrefix = ""
			}
			tagVersion := path.Base(t2)
			if v2, err = semver.NewSemver(tagVersion); err != nil {
				v2 = &semver.Version{}
			}
		}
	}

	return v1.LessThan(v2)
}

// Swap swaps the elements with indexes i and j.
// required to implement sortable interface https://golang.org/pkg/sort/#Interface
func (v Tags) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}
