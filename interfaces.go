package git

import (
	"net/http"

	actions "gitlab.com/digitalxero/simple-actions"
)

type GitClient interface {
	WithDomain(string) GitClient
	WithHTTPClient(*http.Client) GitClient
	WithAuth(Authenticator) GitClient
}

// Authenticator Auth-s the initial connection then allows validation at any point of the stream
type Authenticator interface {
	Type() GitAuth
	User() string
	Password() string
}

type gitActionContext interface {
	actions.ActionContext
	Client() interface{}
	WithClient(GitClient) gitActionContext
	Tags() Tags
	WithTags(Tags) gitActionContext
	Commits() Commits
	WithCommits(Commits) gitActionContext
}