package git

import (
	"fmt"
	"net/http"
	"path"

	semver "github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
)
func (t *Tags) FromGitlab(glTags []*gitlab.Tag, client *gitlab.Client) {
	for _, gt := range glTags {
		tagPrefix := path.Dir(gt.Name)
		if tagPrefix == "." {
			tagPrefix = ""
		}
		tagVersion := path.Base(gt.Name)
		version, _ := semver.NewVersion(tagVersion)
		*t = append(*t, &Tag{
			SHA:     gt.Commit.ID,
			Name:    gt.Name,
			Version: version,
		})
	}
}

func (c *Commits) FromGitlab(glCommits []*gitlab.Commit, client *gitlab.Client) {
	for _, gc := range glCommits {
		var files []string
		if diffs, _, err := client.Commits.GetCommitDiff(gc.ProjectID, gc.ID, nil, nil); err == nil {
			for _, diff := range diffs {
				files = append(files, diff.NewPath, diff.OldPath)
			}
		}
		*c = append(*c, &Commit{
			SHA:     gc.ID,
			Message: gc.Message,
			Files:   files,
			Time:    gc.CommittedDate,
		})
	}
}

type GitlabClient interface {
	Client() (*gitlab.Client, error)
}

type gitlabClient struct {
	domain string
	auth Authenticator
	httpClient    *http.Client
	client        *gitlab.Client
}
func (gl *gitlabClient) WithDomain(domain string) GitClient {
	gl.domain = domain

	return gl
}

func (gl *gitlabClient) WithHTTPClient(httpClient *http.Client) GitClient {
	gl.httpClient = httpClient

	return gl
}

func (gl *gitlabClient) WithAuth(auth Authenticator) GitClient {
	gl.auth = auth

	return gl
}

func (gl *gitlabClient) Client() (*gitlab.Client, error) {
	var err error
	if gl.client == nil {
		if gl.httpClient == nil {
			gl.httpClient = &http.Client{}
		}
		switch gl.auth.Type() {
		case NoAuth:
			gl.client, err = gitlab.NewClient("",
				gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/%s", gl.domain, DefaultAPIVersion)),
				gitlab.WithHTTPClient(gl.httpClient),
			)
		case BasicAuth:
			gl.client, err = gitlab.NewBasicAuthClient(gl.auth.User(), gl.auth.Password(),
				gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/%s", gl.domain, DefaultAPIVersion)),
				gitlab.WithHTTPClient(gl.httpClient),
			)
		case NetRCAuth:
			gl.client, err = gitlab.NewClient(gl.auth.Password(),
				gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/%s", gl.domain, DefaultAPIVersion)),
				gitlab.WithHTTPClient(gl.httpClient),
			)
		case OAuth:
			err = fmt.Errorf("not implemented")
		}
	}
	return gl.client, err
}

// NewGitlabClient return a gitlab GitlabClient that uses the local .netrc for the specified domain
func NewGitlabClient() GitClient {
	return &gitlabClient{}
}