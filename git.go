package git

import (
	"sigs.k8s.io/kind/pkg/log"

	"gitlab.com/digitalxero/simple-actions/term"
)

var (
	DefaultAPIVersion = "v4"
)

type baseGitAction struct {
	projectID,
	branch,
	subprojectName string
}


type defaultGitActionContex struct {
	logger log.Logger
	status *term.Status
	dryRun bool
	tags   Tags
	commits Commits
	client interface{}
}

// NewDefaultActionContext generate a default action context with a nil return for Data()
func NewDefaultActionContext(logger log.Logger, dryRun bool) gitActionContext {
	return &defaultGitActionContex{
		logger: logger,
		status: term.StatusForLogger(logger),
		dryRun: dryRun,
	}
}

// Logger returns the logger
func (ac *defaultGitActionContex) Logger() log.Logger {
	return ac.logger
}

// Status returns the term.Status
func (ac *defaultGitActionContex) Status() *term.Status {
	return ac.status
}

// IsDryRun defines if this should be a dry run
func (ac *defaultGitActionContex) IsDryRun() bool {
	return ac.dryRun
}

// Data returns nil
func (ac *defaultGitActionContex) Data() interface{} {
	return nil
}

// Client returns client
func (ac *defaultGitActionContex) Client() interface{} {
	return ac.client
}

// WithClient sets client
func (ac *defaultGitActionContex) WithClient(client GitClient) gitActionContext {
	ac.client = client
	return ac
}

// Tags returns the current tags in this context
func (ac *defaultGitActionContex) Tags() Tags {
	return ac.tags
}

// WithTags sets the current tags in this context
func (ac *defaultGitActionContex) WithTags(tags Tags) gitActionContext {
	ac.tags = tags
	return ac
}

// Tags returns nil
func (ac *defaultGitActionContex) Commits() Commits {
	return ac.commits
}

// Tags returns nil
func (ac *defaultGitActionContex) WithCommits(commits Commits) gitActionContext {
	ac.commits = commits
	return ac
}