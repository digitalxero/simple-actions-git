package git

import (
	"fmt"

	"github.com/jdxcode/netrc"
)

type noAuth struct {}

func NewNoAuth() (*noAuth, error) {
	return &noAuth{}, nil
}

func (a *noAuth) Type() GitAuth {
	return NoAuth
}

func (a *noAuth) User() string {
	return ""
}

func (a *noAuth) Password() string {
	return ""
}

type netRCAuth struct {
	netrc, domain string
	netrcData    *netrc.Netrc
	netrcMachine *netrc.Machine
}

func NewNetRCAuth(netrcFile, domain string) (n *netRCAuth, err error) {
	n = &netRCAuth{
		netrc:  netrcFile,
		domain: domain,
	}

	if n.netrcData, err = netrc.Parse(netrcFile); err != nil {
		return nil, err
	}
	if n.netrcMachine = n.netrcData.Machine(domain); n.netrcMachine == nil {
		return nil, fmt.Errorf("missing .netrcFile entry for %s", domain)
	}

	return n, nil
}

func (a *netRCAuth) Type() GitAuth {
	return NetRCAuth
}

func (a *netRCAuth) User() string {
	return a.netrcMachine.Get("user")
}

func (a *netRCAuth) Password() string {
	return a.netrcMachine.Get("password")
}

type basicAuth struct {
	user, password string
}

func NewBasicAuth(user, password string) (*basicAuth, error) {
	return &basicAuth{
		user: user,
		password: password,
	}, nil
}

func (a *basicAuth) Type() GitAuth {
	return BasicAuth
}

func (a *basicAuth) User() string {
	return a.user
}

func (a *basicAuth) Password() string {
	return a.password
}