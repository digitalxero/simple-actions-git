package git_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	gitActions "gitlab.com/digitalxero/simple-actions-git"
)

func TestGetAllCommitsGitlabSingleProjectNoPreRelease(t *testing.T) {
	// https://gitlab.com/digitalxero/semrel-test-repo-1
	gitAuth, err := gitActions.NewNoAuth()
	require.NoError(t, err)
	gitClient := gitActions.NewGitlabClient().WithDomain(gitlabDomain).WithAuth(gitAuth)
	actionCTX := gitActions.NewDefaultActionContext(logger, false).WithClient(gitClient)

	getCommitsAction := gitActions.NewGetAllCommits("23365984")
	err = getCommitsAction.Execute(actionCTX)
	require.NoError(t, err)
	commits := actionCTX.Commits()
	require.Len(t, commits, 484)
}
